# fusilli.io-renamer

## Introduction

The job renamer renames an attribute present in the configuration, and inserts it into the json sent to the kafka queue, consisting of header and data
```bash
{"header":{"status":"running"},"data":{"PROVINCIA":"FT","COMUNE":"Sutri","CODICE_INTERVENTO":"LARSFASUTRSUTRI2","DESCRIZIONE_INTERVENTO":"Sutri - Sutri 2","LUNGHEZZA_TRATTA_FO":530,"POSSESSORE_FO":"REGIONE LAZIO"}}
```

The job has an additional function that concerns policies. This function encrypt the data, the attributes present in the configuration, and insert them in the json sent to the kafka queue, consisting of headers and data
```bash
{"header":{"status":"running"},"data":{"PROVINCIA":"FT","COMUNE":"Sutri","CODICE_INTERVENTO":"LARSFASUTRSUTRI2","DESCRIZIONE_INTERVENTO":"Sutri - Sutri 2","LUNGHEZZA_TRATTA_FO":530,"POSSESSORE_FO":"902938"}}
```

At the end of the job the message sent to the queue will be the following:

```bash
{"header":{"status":"end"},"data":""}
```

## How to install scala

Download the Scala and follow the further instructions for the installation of Scala. However, one can easily install latest version of Scala on Ubuntu with the use of following command:

```bash
sudo apt-get install scala
```

## How to install sbt

Download sbt follow the steps in the official site documentation:

```bash
https://www.scala-sbt.org/
```

## Execution of job via IDE

First step is to expose all enviroment variables:

```bash
export input_edges='{"input_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export output_edges='{"output_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export job_config='[{"input_attrs":["PROPRIETARIO_FO"],"output_attrs":["POSSESSORE_FO"],"config":{}}]'

export policies='[{"entity_id": 1, "attrs": [{"attr_name": "POSSESSORE_FO", "policy_behaviour": "tokenization", "parameter" : "6"}]}]'
```

To test locally the project in debug mode run the following command inside the project folder:

```bash
sbt -jvm-debug 5005 run
```

## Execution of job via Docker

Run the following command inside the project folder:

```bash
docker-compose up -f docker/docker-compose.yml
```

This is an example file of docker-compose.yml:

```bash
version: '2.0'
services:
    concatterjob:
        container_name: fusilli_concatter
        image: registry.gitlab.com/kdm-fusilli/fusilli.io-manager/fusilli.io-jobs/fusilli.io-concatter:latest
        restart: on-failure
        command: bash -c "sbt run"
        environment:
            input_edges: '{"input_edges":[{"label":"String","host":"localhost","port":9092,"group_id":"test","topic":"read","user":"String","password":"String"}]}'
            output_edges: '{"output_edges":[{"label":"String","host":"localhost","port":9092,"group_id":"test","topic":"write","user":"String","password":"String"}]}'
            job_configs: '[{"input_attrs":["PROVINCIA","COMUNE"],"output_attrs":["RESIDENZA"],"config":{"spacing":" - "}}]'
            policies: '[{"entity_id": 1, "attrs": [{"attr_name": "POSSESSORE_FO", "policy_behaviour": "tokenization", "parameter" : "6"}]}]'
        ports:
          - 8080:8080
        network_mode: "host"
```

To check the environment variables set in a docker image use the following command:

```bash
docker exec <continer_id> env
```

To check the container logs use the following command:

```bash
docker logs <continer_id>
```
