import Library._

organization in ThisBuild := "io.fusilli"
version in ThisBuild := "0.1.0"
name := "fusilli.io-renamer"

scalaVersion in ThisBuild := "2.13.5"

def dockerSettings = Seq(
  dockerBaseImage := "adoptopenjdk/openjdk8",
  dockerRepository := sys.props.get("docker.registry")
)

libraryDependencies ++= Seq(
  Slf4j,
  AkkaStream,
  akkaStreamAlpakkaSlick,
  KafkaClient,
  PlayLib,
  Hasher
)

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
