package io.fusilli.renamer

import io.fusilli.renamer.classes.Classes.RenameConfig
import io.fusilli.renamer.config.ConfigApp
import io.fusilli.renamer.services.Renamer
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.JsArray

object Main extends App {
  private final val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  val inputQueue = ConfigApp.getInputConfigJson()
  val outputQueue = ConfigApp.getOutputConfigJson()
  val configs = ConfigApp.getJobConfigJson()
  var policies = JsArray.empty

  try {
    policies = ConfigApp.getPoliciesConfigJson()
  } catch {
    case n: NullPointerException => log.warn("Policies not valued")
  }

  val configuration: List[RenameConfig] = (configs.value.map(_.as[RenameConfig])).toList

  Renamer.rename(configuration, policies, inputQueue, outputQueue)

}