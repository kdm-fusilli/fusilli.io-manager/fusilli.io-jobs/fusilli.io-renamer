package io.fusilli.renamer.config;

public enum Constants {

    input_edges,
    output_edges,
    job_config,
    policies,
    log_queue
}