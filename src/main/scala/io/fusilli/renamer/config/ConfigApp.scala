package io.fusilli.renamer.config

import io.fusilli.renamer.classes.Classes.FusilliQueue
import io.fusilli.renamer.config.Constants._
import play.api.libs.json._

object ConfigApp {


  def getKafkaLogHost(): JsValue = {
    val input_edges_str = System.getenv(log_queue.toString)
    val json: JsValue = Json.parse(input_edges_str)
    //TODO: return host:port for log connection
    json
  }

  def getInputConfigJson(): FusilliQueue = {
    val input_edges_str = System.getenv(input_edges.toString)
    val json: FusilliQueue = (Json.parse(input_edges_str) \ "input_edges").as[JsArray].apply(0).as[FusilliQueue]
    json
  }

  def getOutputConfigJson(): FusilliQueue = {
    val output_edges_str = System.getenv(output_edges.toString)
    val json: FusilliQueue = (Json.parse(output_edges_str) \ "output_edges").as[JsArray].apply(0).as[FusilliQueue]
    json
  }

  def getJobConfigJson(): JsArray = {
    val job_configs_str = System.getenv(job_config.toString)
    val json: JsArray = (Json.parse(job_configs_str)).as[JsArray]
    json
  }

  def getPoliciesConfigJson(): JsArray = {
    val policies_configs_str = System.getenv(policies.toString)
    val json: JsArray = (Json.parse(policies_configs_str)).as[JsArray]
    json
  }
}