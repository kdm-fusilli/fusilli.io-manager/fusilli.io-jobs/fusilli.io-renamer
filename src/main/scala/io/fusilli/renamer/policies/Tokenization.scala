package io.fusilli.renamer.policies

import org.slf4j.{Logger, LoggerFactory}

object Tokenization {
  private final val log: Logger = LoggerFactory.getLogger("Policies - Tokenization")

  def tokenization(value: String, parameter: Int): String = {
    val maxLeng = value.length

    //takes a random character from the string
    val refString: String = "0123456789"
    //create a stringbuffer of length 'parameter'
    val sb: StringBuilder = new StringBuilder()
    if (parameter > maxLeng) {
      for (i <- 0 until maxLeng) {
        //0 to 'maxLeng'
        val index: Int = (refString.length * Math.random()).toInt
        //adds one character at a time to the end of sb
        sb.append(refString.charAt(index))
      }
    } else {
      for (i <- 0 until parameter) {
        //0 to 'parameter'
        val index: Int = (refString.length * Math.random()).toInt
        //adds one character at a time to the end of sb
        sb.append(refString.charAt(index))
      }
    }
    //returns sb as a string
    sb.toString
  }
}