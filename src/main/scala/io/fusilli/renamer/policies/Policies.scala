package io.fusilli.renamer.policies

import io.fusilli.renamer.classes.Classes.{PolicyAttribute, PolicyConfig}
import io.fusilli.renamer.policies.Encryption.encryption
import io.fusilli.renamer.policies.Tokenization.tokenization
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsObject, JsString}

object Policies {
  private final val log: Logger = LoggerFactory.getLogger("Policies")

  def policy(data: JsObject, configs: PolicyConfig): Unit = {

    log.info("Executing policies...")

    policyFun(data, configs.attrs)
    log.info("Policies Executed.")
  }

  def policyFun(doc: JsObject, policyAttributes: Seq[PolicyAttribute]): JsObject = {
    var newDoc = doc
    for (p <- policyAttributes) {
      p.policy_behaviour match {
        case "tokenization" =>
          val tmp = tokenization(p.attr_name, p.parameter.toInt)
          newDoc = newDoc - p.attr_name + (p.attr_name -> JsString(tmp))
        case "encryption" =>
          val tmp = encryption(p.attr_name, p.parameter)
          newDoc = newDoc - p.attr_name + (p.attr_name -> JsString(tmp))
        case _ =>
          log.error(s"Policy behaviour not recognized: ${p.policy_behaviour}")
      }
    }
    newDoc
  }
}