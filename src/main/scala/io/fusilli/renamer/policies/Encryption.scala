package io.fusilli.renamer.policies

import com.roundeights.hasher.Implicits._
import org.slf4j.{Logger, LoggerFactory}

object Encryption {
  private final val log: Logger = LoggerFactory.getLogger("Policies - Encryption")

  def encryption(value: String, parameter: String): String = {
    try {
      // check if the salt length is between 6 and 12
      require(6 to 12 contains (parameter.length))
      // if it satisfies the previous requirement then it applies the SHA256 algorithm to the specified value to be encrypted
      value.salt(parameter).sha256.hex
    }
    catch {
      // if the length of sale requirement is not met the error and does not encrypt the value
      case _: Throwable => log.error(s"Salt must have between 6 to 12 characters")
        value
    }
  }
}