package io.fusilli.renamer.services

import io.fusilli.renamer.classes.Classes.{FusilliQueue, JsonMessage, PolicyConfig, RenameConfig}
import io.fusilli.renamer.policies.Policies.policyFun
import io.fusilli.renamer.services.KafkaServices.{getKafkaConsumer, getKafkaProducer}
import kafka.common.KafkaException
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.config.ConfigException
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsArray, JsObject, JsString, Json}

import java.io.FileNotFoundException
import java.time.Duration
import java.util
import scala.jdk.CollectionConverters.IterableHasAsScala

object Renamer {
  private final val log: Logger = LoggerFactory.getLogger("Renamer")

  def rename(configs: List[RenameConfig], policies: JsArray, inputQueue: FusilliQueue, outputQueue: FusilliQueue): Unit = {
    log.info("Executing job...")

    try {

      val consumer: KafkaConsumer[String, String] = getKafkaConsumer(inputQueue.host, inputQueue.port, inputQueue.group_id)

      val producer: KafkaProducer[String, String] = getKafkaProducer(outputQueue.host, outputQueue.port)

      consumer.subscribe(util.Arrays.asList(inputQueue.topic))

      var cycle = true

      while (cycle) {
        val record = consumer.poll(Duration.ofMillis(1000)).asScala //this is used to read a line in the queue
        for (data <- record.iterator) {
          log.info("Row retrieved.")
          val jsonMessage = Json.parse(data.value()).as[JsonMessage]
          var newJsonMessage = ""

          if (!jsonMessage.header.status.equals("end")) {
            log.info("Executing job...")
            var row = JsObject.empty
            val rowRename = renameFun(jsonMessage.data.as[JsObject], configs)
            if (policies != JsArray.empty) {
              row = policyFun(rowRename, policies.apply(0).as[PolicyConfig].attrs)
            } else {
              row = rowRename
            }
            newJsonMessage = Json.toJson(new JsonMessage(jsonMessage.header, row)).toString
          } else {
            newJsonMessage = Json.toJson(jsonMessage).toString
            log.info("Executing end...")
            cycle = false
          }
          log.info("Job Executed. Sending row to Kafka...")
          producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", newJsonMessage))
        }
      }
    } catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
    }
  }

  def renameFun(doc: JsObject, jobConfigs: List[RenameConfig]): JsObject = {
    var newDoc = doc
    for (j <- jobConfigs) {
      val newVal = (newDoc \ j.input_attrs(0)).as[String]
      newDoc = newDoc - j.input_attrs(0) + (j.output_attrs(0) -> JsString(newVal))

    }
    newDoc
  }
}