package io.fusilli.renamer.classes

import play.api.libs.json.{Format, JsValue, Json}

object Classes {

  case class FusilliQueue(
                           host: String,
                           port: Int,
                           group_id: String,
                           topic: String,
                           user: String,
                           password: String
                         )

  object FusilliQueue {
    implicit val format: Format[FusilliQueue] = Json.format[FusilliQueue]
  }

  case class RenameConfig(
                            input_attrs: List[String],
                            output_attrs: List[String],
                            config: JsValue
                          )

  object RenameConfig {
    implicit val format: Format[RenameConfig] = Json.format[RenameConfig]
  }

  case class JsonMessage(
                          header: Header,
                          data: JsValue,
                        )

  object JsonMessage {
    implicit val format: Format[JsonMessage] = Json.format[JsonMessage]
  }

  case class Header(
                     status: String
                   )

  object Header {
    implicit val format: Format[Header] = Json.format[Header]
  }

  case class PolicyConfig(
                           entity_id: Int,
                           attrs: Seq[PolicyAttribute]
                         )

  object PolicyConfig {
    implicit val format: Format[PolicyConfig] = Json.format[PolicyConfig]
  }

  case class PolicyAttribute(
                              attr_name: String,
                              policy_behaviour: String,
                              parameter : String
                            )
  object PolicyAttribute{
    implicit val format: Format[PolicyAttribute] = Json.format[PolicyAttribute]
  }

}
