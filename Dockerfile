FROM openjdk:8
ENV SBT_VERSION 1.4.7

RUN \
    curl -L -o sbt-$SBT_VERSION.deb http://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
    dpkg -i sbt-$SBT_VERSION.deb && \
    rm sbt-$SBT_VERSION.deb && \
    apt-get update && \
    apt-get install sbt && \
    sbt -Dsbt.rootdir=true sbtVersion

WORKDIR /fusilli-io-renamer

COPY . /fusilli-io-renamer
CMD sbt run