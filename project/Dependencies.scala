import sbt._

// format: OFF
object Version {

  val Log4j                         = "1.7.30"
  val Akka                          = "2.6.9"
  val KafkaClients                  = "2.7.0"
  val akkaStreamAlpakkaSlick        = "2.0.1"
  val Play                          = "2.9.2"
  val HasherOutr                    = "1.2.2"

  //val OrgPostgres                   = "42.2.11"
}

object Library {
  // External Common Libraries
  val Slf4j                       = "org.slf4j"                       % "slf4j-simple"                       % Version.Log4j
  val AkkaStream                  = "com.typesafe.akka"               %% "akka-stream"                       % Version.Akka
  val akkaStreamAlpakkaSlick      = "com.lightbend.akka"              %% "akka-stream-alpakka-slick"         % Version.akkaStreamAlpakkaSlick
  val KafkaClient                 = "org.apache.kafka"                %% "kafka"                             % Version.KafkaClients
  val PlayLib                     = "com.typesafe.play"               %% "play-json"                         % Version.Play
  val Hasher                      = "com.outr"                        %% "hasher"                            % Version.HasherOutr


  // External Specific Libraries

  // TODO: put here all libraries
  // val OrgPostgres             = "org.postgresql"                  % "postgresql"                         % Version.OrgPostgres
}
// format: ON
// ,
//  "